# Copyright (C) 2023 Mauro Aranda

# This file is part of cus-dir.

# cus-dir is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# cus-dir is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with cus-dir.  If not, see <https://www.gnu.org/licenses/>.

## Programs used.
EMACS = emacs
EMACSFLAGS = -batch -f batch-byte-compile

## Variables (some might not be used right now).
PACKAGE = cus-dir
PACKAGE_BUGREPORT = maurooaranda@gmail.com
PACKAGE_NAME = cus-dir
PACKAGE_STRING = cus-dir 1.0
PACKAGE_TARNAME = cus-dir-1.0
PACKAGE_VERSION = 1.0
DISTDIR = $(PACKAGE_TARNAME)
DISTFILES = Makefile cus-dir.el

## Targets.

.PHONY: all clean dist

all: cus-dir.elc

cus-dir.elc: cus-dir.el
	$(EMACS) $(EMACSFLAGS) cus-dir.el

clean:
	-rm -f cus-dir.elc
	-rm -f $(PACKAGE_TARNAME).tar.gz

dist: cus-dir.elc
	mkdir --parents $(DISTDIR)
	cp --parents $(DISTFILES) $(DISTDIR)
	tar -cf $(PACKAGE_TARNAME).tar $(DISTDIR)
	rm -R $(DISTDIR)
	gzip $(PACKAGE_TARNAME).tar
