;;; cus-dir.el --- Easy Customization for .dir-locals files  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Mauro Aranda

;; Author: Mauro Aranda <maurooaranda@gmail.com>
;; Maintainer: Mauro Aranda <maurooaranda@gmail.com>
;; Created: Sat Sep 23 20:00:00 2023
;; Version: 0.1
;; Package-Version: 0.1
;; Package-Requires: ((emacs "25.1"))
;; URL: https://gitlab.com/mauroaranda/cus-dir
;; Keywords: convenience

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; Easy Customization interface to manage `.dir-locals.el' files.
;; To customize a `.dir-locals.el' file for the current directory, type:
;; M-x customize-dirlocals
;; Edit the values you want to have in the `.dir-locals.el' file, and when
;; you are finished, type C-x C-s.
;; To cancel edits, just kill the buffer.
;; There's no undo or resetting for now.  If you mess up, you have to fix it
;; by yourself, or kill the buffer and start again.
;; When you add a new variable, type RET in the field, so that the field that
;; holds the value updates, to show an appropriate field to edit the value,
;; like Customize would do for the variable.
;;
;; For more information on Directory Variables, see
;; (info "(Emacs) Directory Variables")
;; For more information on Customize, see
;; (info "(Emacs) Easy Customization")

;;; Code:

;; Requirements.
(require 'wid-edit)
(require 'cus-edit)
(require 'files-x) ; For `dir-locals-to-string'.
(declare-function project-current "project.el")
(declare-function project-root "project.el")
(defvar auto-insert) ; To avoid warning.

;; Utils.
(defun custom-dirlocals--editable-field-p (widget)
  "Non-nil if WIDGET is an editable-field widget, or inherits from it."
  (let ((type (widget-type widget)))
    (while (and type (not (eq type 'editable-field)))
      (setq type (widget-type (get type 'widget-type))))
    type))

;; User options.
(defgroup custom-dirlocals nil
  "Customize Group for `custom-dirlocals'."
  :group 'customize
  :version "0.1")

(defcustom custom-dirlocals-insert-dirlocals2-header nil
  "Whether to insert a header in the `.dir-locals-2.el' file.

When t, the header is similar to the one that `modify-dir-local-variable'
adds to the `.dir-locals.el' file."
  :type 'boolean
  :package-version '(cus-dir . "0.1"))

;; Variables.
(defvar-local custom-dirlocals-widget nil
  "Hold the widget with the dirlocals customizations.")

(defvar-local custom-dirlocals-file-widget nil
  "Widget that holds the name of the `.dir-locals.el' file being customized.")

(defvar-keymap custom-dirlocals-map
  :doc "Keymap used in the buffer to customize `.dir-locals.el' files."
  :full t
  :parent widget-keymap
  "C-x C-s" #'Custom-dirlocals-save)

(defvar custom-dirlocals-field-map
  (let ((map (copy-keymap custom-field-keymap)))
    (define-key map "\C-x\C-s" #'Custom-dirlocals-save)
    (define-key map "\C-m" #'widget-field-activate)
    map)
  "Keymap for the editable fields in the \"*Customize dirlocals*\" buffer .")

;; Widgets.
(define-widget 'custom-dirlocals-key 'menu-choice
  "Menu to choose between possible keys in a dir-locals file.

Possible values are nil, a symbol (standing for a major mode) or a directory
name."
  :tag "Specification"
  :value nil
  :help-echo "Select a key for the dir-locals specification."
  :args '((const :tag "All modes" nil)
          (symbol :tag "Major mode" fundamental-mode)
          (directory :tag "Subdirectory")))

(define-widget 'custom-dirlocals-dynamic-cons 'cons
  "A cons widget that changes its 2nd type based on the 1st type."
  :value-create #'custom-dirlocals-dynamic-cons-value-create)

(defun custom-dirlocals-dynamic-cons-value-create (widget)
  "Select an appropriate cdr type for the cons WIDGET and create WIDGET.

The appropriate types are:
- A symbol, if the value to represent is a minor-mode.
- A boolean, if the value to represent is either the unibyte value or the
  subdirs value.
- A widget type suitable for editing a variable, in case of specifying a
  variable's value.
- A sexp widget, if none of the above happens."
  (let* ((args (widget-get widget :args))
         (value (widget-get widget :value))
         (val (car value)))
    (cond
     ((eq val 'mode) (setf (nth 1 args)
                           '(symbol :keymap custom-dirlocals-field-map
                                    :tag "Minor mode")))
     ((eq val 'unibyte) (setf (nth 1 args) '(boolean)))
     ((eq val 'subdirs) (setf (nth 1 args) '(boolean)))
     ((custom-variable-p val)
      (let ((w (widget-convert (custom-variable-type val))))
        (when (custom-dirlocals--editable-field-p w)
          (widget-put w :keymap custom-dirlocals-field-map))
        (setf (nth 1 args) w)))
     (t (setf (nth 1 args) '(sexp :keymap custom-dirlocals-field-map))))
    (widget-put (nth 0 args) :keymap custom-dirlocals-field-map)
    (widget-group-value-create widget)))

(define-widget 'custom-dirlocals 'editable-list
  "An editable list to edit specifications in a `.dir-locals.el' file."
  :entry-format "\n%i %d %v"
  :insert-button-args '(:help-echo "Insert new specification here.")
  :append-button-args '(:help-echo "Append new specification here.")
  :delete-button-args '(:help-echo "Delete this specification.")
  :args '((group :format "%v"
                 custom-dirlocals-key
                 (repeat
                  :tag "Settings"
                  :inline t
                  (custom-dirlocals-dynamic-cons
                   :tag "Setting"
                   (symbol :action Custom-dirlocals-symbol-action
                           :custom-dirlocals-symbol t)
                   ;; Will change dynamically.
                   (sexp :tag "Value"))))))

(defun Custom-dirlocals-symbol-action (widget &optional _event)
  "Action for the symbol WIDGET.

Sets the value of its parent, a cons widget, in order to create an
appropriate widget to edit the value of WIDGET.

Moves point into the widget that holds the value."
  (interactive)
  (setq widget (or widget (widget-at)))
  (widget-value-set (widget-get widget :parent)
                    (cons (widget-value widget) ""))
  (widget-setup)
  (widget-forward 1))

(defun custom-dirlocals-change-file (widget &optional _event)
  "Switch to a buffer to customize the `.dir-locals.el' file in WIDGET."
  (customize-dirlocals (expand-file-name (widget-value widget))))

;; Functions.
(defun custom-dirlocals--set-widget-vars ()
  "Set local variables for the Widget library."
  (add-hook 'widget-forward-hook #'custom-dirlocals-maybe-update-cons nil t)
  (setq-local widget-button-face custom-button)
  (setq-local widget-button-pressed-face custom-button-pressed)
  (setq-local widget-mouse-face custom-button-mouse)
  (when custom-raised-buttons
    (setq-local widget-push-button-prefix "")
    (setq-local widget-push-button-suffix "")
    (setq-local widget-link-prefix "")
    (setq-local widget-link-suffix "")))

(defmacro custom-dirlocals-with-buffer (&rest body)
  "Arrange to execute BODY in a \"*Customize dirlocals*\" buffer."
  `(progn
     (switch-to-buffer "*Customize dirlocals*")
     (kill-all-local-variables)
     (let ((inhibit-read-only t))
       (erase-buffer))
     (remove-overlays)
     (custom-dirlocals--set-widget-vars)
     ,@body
     (setq-local revert-buffer-function #'custom-dirlocals--revert-buffer)
     (use-local-map custom-dirlocals-map)
     (widget-setup)))

(defun custom-dirlocals--revert-buffer (&rest _ignored)
  "Revert the buffer for Directory Local Variables customization."
  (customize-dirlocals (widget-value custom-dirlocals-file-widget)))

(defun custom-dirlocals-maybe-update-cons ()
  "If focusing out from the first widget in a cons widget, update its value."
  (when-let ((w (widget-at)))
    (when (widget-get w :custom-dirlocals-symbol)
      (widget-value-set (widget-get w :parent)
                    (cons (widget-value w) ""))
      (widget-setup))))

(defun custom-dirlocals-edit-dirlocals2 (file settings)
  "Edit SETTINGS in the `.dir-locals-2.el' FILE.

This function exists because there's no way to use `add-dir-local-variable'
or `delete-dir-local-variable' to modify the `.dir-locals-2.el' file instead
of the main `.dir-locals.el' file.

This function doesn't work like `modify-dir-local-variable'.  It just deletes
the contents in FILE and recreates them with SETTINGS."
  (unless enable-local-variables
    (error "Directory-local variables are disabled"))
  (let ((auto-insert nil))
    (find-file file)
    (widen)
    (delete-region (point-min) (point-max))
    (goto-char (point-min))
    (when custom-dirlocals-insert-dirlocals2-header
      (insert ";;; Alternative Directory Local Variables  -*- no-byte-compile: t -*-\n"
              ";;; For more information see (info \"(emacs) Directory Variables\")\n\n"))
    (princ (dir-locals-to-string
            (sort settings
		  (lambda (a b)
		    (cond
		     ((null (car a)) t)
		     ((null (car b)) nil)
		     ((and (symbolp (car a)) (stringp (car b))) t)
		     ((and (symbolp (car b)) (stringp (car a))) nil)
		     (t (string< (car a) (car b)))))))
           (current-buffer))
    (when (eobp) (insert "\n"))
    (goto-char (point-min))
    (indent-sexp)))

;; Commands.
(defun Custom-dirlocals-save (&rest _ignore)
  "Save the settings to the `.dir-locals.el' file."
  (interactive)
  (let ((file (widget-value custom-dirlocals-file-widget)))
    (if (string-match-p "2.el$" file)
        (custom-dirlocals-edit-dirlocals2
         file (widget-value custom-dirlocals-widget))
      (let ((old (widget-get custom-dirlocals-widget :value))
            (dirlocals (widget-value custom-dirlocals-widget)))
        (dolist (spec old)
          (let ((mode (car spec))
                (settings (cdr spec)))
            (dolist (setting settings)
              (delete-dir-local-variable mode (car setting)))))
        (dolist (spec dirlocals)
          (let ((mode (car spec))
                (settings (cdr spec)))
            (dolist (setting (reverse settings))
              (when (memq (car setting) '(mode eval))
                (delete-dir-local-variable mode (car setting)))
              (add-dir-local-variable mode (car setting) (cdr setting))))))))
  (write-file (expand-file-name buffer-file-name) nil)
  (kill-buffer))

;;;###autoload
(defun customize-dirlocals-project ()
  "Customize the `.dir-locals.el' file for a project.

Defaults to the current project, as `project-current' would return.  If not
in a project, prompts for the project root."
  (interactive)
  (when-let* ((proj (project-current t))
              (default-directory (project-root proj)))
    (customize-dirlocals)))

;;;###autoload
(defun customize-dirlocals (&optional filename)
  "Customize the `.dir-locals.el' file.

With optional argument FILENAME non-nil, customize the `.dir-locals.el' file
that FILENAME specifies."
  (interactive)
  (custom-dirlocals-with-buffer
   (let* ((file (or filename (expand-file-name ".dir-locals.el")))
          (dirlocals (when (file-exists-p file)
                       (with-current-buffer (find-file-noselect file)
                         (goto-char (point-min))
                         (prog1 (read (current-buffer))
                           (kill-buffer))))))
     (widget-insert
      "This buffer is for customizing the dirlocals variables in:\n")
     (setq custom-dirlocals-file-widget
           (widget-create
            `(file :action ,#'custom-dirlocals-change-file
                   ,file)))
     (widget-insert
      (concat
       "\n\nTo select another .dir-locals.el file, edit the above field\n"
       "and hit RET, so that the buffer updates the values.\n\n"
       "After you enter a user option name under the symbol field,\n"
       "be sure to press RET, so that the field that holds the\n"
       "value changes to an appropriate field for the option.\n\n"
       "Type C-x C-s when you've finished editing it, to save the\n"
       "settings to the file.\n\n\n"))
     (widget-create 'push-button :tag "Revert buffer"
                    :action #'custom-dirlocals--revert-buffer)
     (widget-insert " ")
     (widget-create 'push-button :tag "Save settings"
                    :action #'Custom-dirlocals-save)
     (widget-insert "\n")
     (setq custom-dirlocals-widget
           (widget-create 'custom-dirlocals :value dirlocals))
     (setq default-directory (file-name-directory file))
     (goto-char (point-min)))))

(provide 'cus-dir)
;;; cus-dir.el ends here
