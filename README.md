# cus-dir: Easy Customization for .dir-locals.el files

> Simple package to avoid editing the .dir-locals.el file by hand

## Table of Contents

- [Installation](#installation)
- [Using](#using)
- [License](#license)

---

## Installation
- Download and open up a terminal in the directory.
- Type `make`.
- Add the directory to your load path:

Done!

---

## Using

`M-x customize-dirlocals`

And just modify the variables.  Type `C-x C-s` to save.

---

## License

- **[GPL 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html)**
- Copyright 2023 Mauro Aranda
